var gulp = require('gulp');
    sass = require('gulp-sass');
    browserSync = require('browser-sync');
    del = require('del');
    imagemin = require('gulp-imagemin');
    uglify = require('gulp-uglify');
    usemin = require('gulp-usemin');
    rev = require('gulp-rev');
    cleanCss = require('gulp-clean-css');
    faltmap = require('gulp-flatmap');
    htmlmin = require('gulp-htmlmin');

gulp.task('sass',gulp.series( function(){
    gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
}));

gulp.task('sass:watch',gulp.series( function(){
    gulp.watch('./css/*.scss', ['sass']);
}));

gulp.task('browser-sync',gulp.series( function(){
    var file = ['./*.html','./css/*.css', './img/*.{png,jpg,gif}','./js/*.js']
    browserSync.init(files,{
        server:{
            baseDir:'./'
        }
    });
}));

gulp.task('default', gulp.series(['browser-sync'], function(){
    gulp.setMaxListeners('sass:watch');
}));

gulp.task('clean',gulp.series( function(){
    return del(['dist']);
}));

gulp.task('copyfonts', gulp.series(function(){
    gulp.src('.node_modules/open-iconic/font/fonts/*.{ttf, woff, eof, svg, eot, otf}*')
    .pipe(gulp.dest('.dist/fonts'));
}));

gulp.task('imagemin',gulp.series( function(){
    return gulp.src('./images/*.{png,jpg,jpeg, gif}')
        .pipe(imagemin({optimizationLevel:3, progressive:true, interlaced:true}))
        .pipe(gulp.dest('dist/images'));
}));

gulp.task('usemin',gulp.series( function(){
    return gulp.src('./*.html')
        .pipe(flatmap(function(stream, file){
            return stream   
                .pipe(usemin({
                    css:[rev()],
                    html:[function(){ return htmlmin({collapseWhitespace:true})}],
                    js: [uglify(), rev()],
                    inlinejs:[uglify()],
                    inlinecss:[cleanCss(),  'concat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
}));

gulp.task('build',gulp.series( ['clean'], function(){
    ('copyfonts', 'imagemin', 'usemin');
}));

