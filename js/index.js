$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({interval:6000});
    $('#contacto').on('show.bs.modal', function (variable) {
      console.log('el model se esta mostrando');
    });
    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-primary');
  });